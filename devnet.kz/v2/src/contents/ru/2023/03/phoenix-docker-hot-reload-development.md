---
title: Разработка на Phoenix в Docker среде с горячей перезагрузкой 🔥
short: phoenix-docker-hot-reload-development
description: Разработка на Phoenix в Docker среде с горячей перезагрузкой 🔥
createdAt: 2023-03-06
updatedAt: 2023-03-06
category: elixir
isDraft: false
slug: phoenix-docker-hot-reload-development
author: Amirzhan Aliyev
email: letavocado@proton.me
pin: false
buymeacoffee: https://www.buymeacoffee.com/teamxg
linkedin: https://www.linkedin.com/in/yerlen-zhubangaliyev/
gitlab:
tags:
  - elixir
  - phoenix
  - docker
  - docker-compose
  - hot-reload
image: /images/covers/phoenix-framework-logo.png
license:
  - by
  - nc
language:
  - ru
---

![alt text](/images/covers/elixir+doker+hot-reload-preview.jpg)

# Что такое Phoenix?

---

[Phoenix](https://www.phoenixframework.org/) — веб-фреймворк созданный на функциональном языке программирования [Elixir](https://elixir-lang.org/). Elixir в свою же очередь основан на Erlang, которая использует виртуальную машину [BEAM](https://www.erlang.org/blog/a-brief-beam-primer/) и платформу [OTP(Open Telecom Platform)](https://ru.wikipedia.org/wiki/Open_Telecom_Platform). Phoenix подходит для создания веб-приложении реального времени, обеспечивая отказоустойчивость распределенных систем с минимальной задержкой.

# Установка Phoenix

---

Лучшая инструкция по установке Phoenix находится на сайте [официального руководства](https://hexdocs.pm/phoenix/up_and_running.html).

# Разработка в Docker среде

---

## Создаем Dockerfile

1. В корне проекта нужно создать Dockerfile основанный на [официальном образе elixir](https://hub.docker.com/_/elixir), тем самым избегая установки сред разработок для Elixir и Erlang

```Dockerfile
  FROM elixir:latest
```

2. WORKDIR — задаём рабочую директорию

```Dockerfile
WORKDIR /app
```

3. Импортируем нужные файлы в рабочую директорию

```Dockerfile
ADD . /app
```

4. Обновляем образ и устанавливаем следующие пакеты:

- `npm` - нужен для работы с js зависимостями
- `build-essential` - пакеты необходимы для компиляции
- `inotify-tools` - в Phoenix встроена живая перезагрузка (Live Reloading). Автоматически обновляет страницу в браузере при изменение кода и ресурсов(heex, ex, exs, css, js, images, и т.д.) на сервере. Чтобы эта функция работала, вам нужен наблюдатель за (файловой системой)[https://github.com/inotify-tools/inotify-tools/wiki].

```Dockerfile
RUN apt-get update && apt-get -y install npm build-essential inotify-tools
```

5. Устанавливаем js зависимости внутри директории `assets`

```Dockerfile
RUN npm install --prefix ./assets
```

6. Устанавливаем локальные копии `hex` и `rebar`

- `hex` - пакетный менеджер для экосистемы Erlang
- `rebar` - инструмент для удобного управления проектом
- `--force` - принудительная установка без приглашения оболочки; в первую очередь предназначен для автоматизации в системах сборки, таких как `make`

```Dockerfile
RUN mix local.hex --force && mix local.rebar --force
```

7. Устанавливаем зависимости и компилируем

```Dockerfile
RUN mix do deps.get, compile
```

8. Указываем порт 4000

```Dockerfile
EXPOSE 4000
```

9. Запускаем `bash`-скрипт в корне проекта

```Dockerfile
CMD ["./dev"]
```

Итоговый Dockerfile

```Dockerfile
FROM elixir:latest

WORKDIR /app

ADD . /app

RUN apt-get update && apt-get -y install npm build-essential inotify-tools
RUN npm install --prefix ./assets
RUN mix local.hex --force && mix local.rebar --force
RUN mix do deps.get, compile

EXPOSE 4000

CMD ["./dev"]
```

## Создаем скрипт для запуска проекта

---

После того, как проект скомпилирован, можно запустить `iex` сессию внутри проекта, запустив приведенный ниже скрипт. `-S mix` необходим для работы в интерактивной оболочке

```bash
#!/bin/sh
exec iex -S mix phx.server
```

## Создаем docker-compose.yml

---

```yml
version: "3.9" # используем свежую версию docker-compose
services:
  phoenix-app:
    container_name: phoenix-container
    restart: always

    build: . # указываем путь до Dockerfile, в нашем случае он в корне проекта

    environment:
      - MIX_ENV=dev # задаем переменные для mix окружения

    # монтируем необходимые файлы, которые буду реагировать на изменения в коде. Нужен для работы горячей перезагрузки
    volumes:
      - ./assets:/app/assets
      - ./priv:/app/priv
      - ./lib:/app/lib
      - ./config:/app/config
    ports:
      - "4000:4000"

    # `tty: true` и `stdin_open: true` необходимы для работы с интерактивной оболочкой. Без этого iex выдаст ошибку
    tty: true
    stdin_open: true
```

## Выводы

---

В данной статье показано, как на практике разрабатывать `Phoenix` проекты в `Docker` среде с горячей перегрузкой.
