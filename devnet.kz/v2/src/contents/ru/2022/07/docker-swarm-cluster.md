---
title: Docker swarm cluster
short: Docker swarm cluster
description: Создадим небольшой кластер для разработки.
createdAt: 2022-07-01
updatedAt: 2022-07-017
category: docker
isDraft: true
slug: docker-swarm-cluster
author: Yerlen Zhubangaliyev
email: yerlen@yerlen.com
pin: false
buymeacoffee: https://www.buymeacoffee.com/teamxg
linkedin: https://www.linkedin.com/in/yerlen-zhubangaliyev/
gitlab: 
tags:
- swarm
- cluster
- docker
- docker-compose
image: /images/covers/docker-logo.svg
license:
- by
- nc
language:
- ru
---

# Docker swarm cluster

install docker on ubutnu 22
https://docs.docker.com/engine/install/ubuntu/

```bash
sudo apt update && sudo apt install ca-certificates curl gnupg lsb-release -y
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
  
sudo apt-get update && sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin -y
```

3 nodes

glusterfs client
```bash
apt update && apt install glusterfs-client -y
```

On node manager
```bash
docker swarm init
```

output
```
Swarm initialized: current node (uh4o4lvyzpsud7coyewpxwhf2) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-4q1wf1ox5zgt1ykxvzpinmnq1yb3ctvbe3jcrfq6swqbypqcji-185826dy1imj3gixxx77sw0sw 10.101.69.11:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```

## Gitlab runner
on manager nodes run gitlab runner

```bash
docker run -d \
    --name gitlab-runner \
    --restart always \
    -v gitlab-runner:/etc/gitlab-runner \
    -v /var/run/docker.sock:/var/run/docker.sock \
    gitlab/gitlab-runner:latest
    
docker exec -it gitlab-runner bash
export GITLAB_URL=https://gitlab.com/
export GITLAB_TOKEN=GR1348941aR9ncBpuUQuik_rxrocb
export GITLAB_TAGS=devnet-cluster-01,production

gitlab-runner \
    register -n \
    --name "Swarm manager 01" \
    --executor docker \
    --docker-image docker:latest \
    --docker-volumes /var/run/docker.sock:/var/run/docker.sock \
    --url $GITLAB_URL \
    --registration-token $GITLAB_TOKEN \
    --tag-list $GITLAB_TAGS
```



## Выводы

В данной мини-статье мы показали как удобнее разрабатывать приложение на Golang с горячей перегрузкой.
