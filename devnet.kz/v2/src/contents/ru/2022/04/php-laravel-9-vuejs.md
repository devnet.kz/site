---
title: Laravel Restful API + Vue.js и Api тестирование
short: Laravel API, Vue.js и Api тесты
description: В данной статье опишем как написать базовый API приложения TodoList в связке с vue.js и покроем тестами API.
createdAt: 2022-04-07
category: laravel
isDraft: true
slug: laravel-restful-api-vuejs-and-api-tests
author: Yerlen Zhubangaliyev
email: yerlen@yerlen.com
pin: false
buymeacoffee: https://www.buymeacoffee.com/teamxg
linkedin: https://www.linkedin.com/in/yerlen-zhubangaliyev/
gitlab: https://gitlab.com/devnet.kz/laravel-api-vue.js-and-api-tests
tags:
- php
- laravel
- vue.js
image: /images/covers/laravel-logo.svg
license:
- by
- nc
language:
- ru
---

## Установка Laravel 9 (Linux/macOS)

Для установки/запуска должен быть установлен Docker engine 20.10.x и не занятый 80/tcp порт.
Будем создавать новый проект ``laravel-api-vue``.
В зависимости добавим только MySQL, доступные сервисы для установки:
* mysql
* pgsql
* mariadb
* redis
* memcached
* meilisearch
* minio
* selenium
* mailhog

На этом шаге будет установлен Laravel 9, все зависимости и запущены контейнеры.
Приложение будет запущено и доступно по адресу ``http://localhost``

```bash
export PROJECT_NANE=laravel-api-vue
curl -s "https://laravel.build/${PROJECT_NANE}?with=mysql&devcontainer" | bash
cd $PROJECT_NANE
./vendor/bin/sail up
```

## Установим пакеты ``laravel/sanctum``, ``laravel/fortify``

Они будут нужны для реализации авторизации/регистрации.

Провалимся в контейнер PHP. Найти нужный контейнер можно будет командой ``docker ps | grep sail``, 
в данном примере мы зайдем по имени контейнера.

```bash
docker exec -it laravel-api-vue-laravel.test-1 bash
```

И выполним.

```bash
composer require laravel/sanctum laravel/fortify
php artisan vendor:publish --provider="Laravel\Sanctum\SanctumServiceProvider"
php artisan vendor:publish --provider="Laravel\Fortify\FortifyServiceProvider"
```

Приведите ``database/seeders/DatabaseSeeder.php`` в следующий вид

```php[database/seeders/DatabaseSeeder.php]
<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User
            ::factory(1)
            ->create(
                [
                    'name'              => 'Devnet',
                    'email'             => 'devnet@devnet.kz',
                    'email_verified_at' => null,
                ]
            )
        ;
    }
}

```

Выполним миграцию.

```bash
php artisan migrate --seed
```

Уберите комментарий  ``//`` в строке ``\Laravel\Sanctum\Http\Middleware\EnsureFrontendRequestsAreStateful::class``

```php[app/Http/Kernel.php]
'api' => [
    // \Laravel\Sanctum\Http\Middleware\EnsureFrontendRequestsAreStateful::class,
    'throttle:api',
    \Illuminate\Routing\Middleware\SubstituteBindings::class,
],
```

Создайте модель ``app/Models/PersonalAccessToken.php``

```php[app/Models/PersonalAccessToken.php]
<?php

namespace App\Models;

use Laravel\Sanctum\PersonalAccessToken as SanctumPersonalAccessToken;

class PersonalAccessToken extends SanctumPersonalAccessToken
{
    
}

```

Приведите ``AppServiceProvider.php`` в вид указанный ниже

```php[app/Providers/AppServiceProvider.php]
<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\PersonalAccessToken;
use Laravel\Sanctum\Sanctum;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Sanctum::usePersonalAccessTokenModel(PersonalAccessToken::class);
    }
}

```

Убедитесь что в модели ``User`` подключен Trait ``Laravel\Sanctum\HasApiTokens``
и реализует контракт ``MustVerifyEmail``

```php[app/Models/User.php]
<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable;
    
    // ...
}
```

Приведите ``app/config/cors.php`` в следующий вид:

```php[app/config/cors.php]
return [
    //...
    'paths' => [
        'api/*',
        'login',
        'logout',
        'register',
        'user/password',
        'forgot-password',
        'reset-password',
        'sanctum/csrf-cookie',
        'user/profile-information',
        'email/verification-notification',
    ],
    //...
    'supports_credentials' => true,
];
```

## Настройки ``laravel/fortify``

Начнём с файла конфигурации, приведем указанные параметры в такой вид:

```php[config/fortify.php]
return [
    // ...
    'views' => false,
    // ...
    'features' => [
        Features::registration(),
        Features::resetPasswords(),
        Features::emailVerification(),
        Features::updateProfileInformation(),
        Features::updatePasswords(),
    ],
];

```

Добавьте ``FortifyServiceProvider::class`` в ``config/app.php``

```php[config/app.php]
return [
    /*
     * Application Service Providers...
     */
    //...
    App\Providers\FortifyServiceProvider::class,
];
```
