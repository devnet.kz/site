// https://nuxt.com/docs/api/configuration/nuxt-config

declare module '@nuxt/schema' {
    interface PublicRuntimeConfig {
        allowedDomains: string
        defaultDomain: string
        domainMap: string
        gtagIdDevnet: string
        gtagIdFoobar: string
        revision: string
        appName: string
    }
}

export default defineNuxtConfig({
    devtools:      {enabled: true},
    ssr:           true,
    srcDir:        "./src",
    modules:       [
        "nuxt-icons",
        "@nuxt/content",
        "nuxt-security",
        "nuxt-lodash",
        "@nuxtjs/robots",
        "nuxt-gtag",
    ],
    plugins:       [
        "@/plugins/domain",
    ],
    app:           {
        buildAssetsDir: '_fd',
        rootId:         '_fd',
        head:           {
            meta: [
                {
                    charset: "utf-8",
                },
                {
                    name:    "viewport",
                    content: "width=device-width, initial-scale=1",
                },
                {
                    "http-equiv": "cache-control",
                    "content":    "no-cache",
                },
            ]
        },
    },
    content:       {
        watch: false,
    },
    typescript:    {
        typeCheck: true
    },
    runtimeConfig: {
        public: {
            allowedDomains: '',
            defaultDomain:  '',
            domainMap:      '',
            gtagIdDevnet:   '',
            gtagIdFoobar:   '',
            revision:       '',
            appName:        '',
        }
    },
    css:           [
        '@/assets/scss/tailwind.scss',
        '@/assets/scss/main.scss',
    ],
    postcss:       {
        plugins: {
            'postcss-import':      {},
            'tailwindcss/nesting': {},
            tailwindcss:           {},
            autoprefixer:          {},
        }
    },
    vite:          {
        build: {
            assetsDir: '_fd',
        },
        css:   {
            preprocessorOptions: {
                scss: {
                    additionalData:
                        '@use "@/assets/scss/_variables.scss" as *;'
                        + '@use "@/assets/scss/_mixins.scss" as *;',
                },
            },
        },
    },
    security:      {
        headers:       {
            crossOriginEmbedderPolicy: process.env.NODE_ENV === 'development' ?
                                           'unsafe-none' : 'require-corp',
            contentSecurityPolicy:     {
                'default-src':               [
                    "'self'"
                ],
                'script-src':                [
                    "'self'",
                    "https:",
                    "'nonce-{{nonce}}'",
                    "https://www.googletagmanager.com",
                    "https://www.google-analytics.com"
                ],
                'style-src':                 [
                    "'self'",
                    "https:",
                    "'unsafe-inline'",
                ],
                'img-src':                   [
                    "'self'",
                    "data:",
                ],
                'font-src':                  [
                    "'self'",
                    "https:",
                    "data:",
                ],
                'base-uri':                  ["'none'"],
                'object-src':                ["'none'"],
                'script-src-attr':           ["'none'"],
                'form-action':               ["'self'"],
                'frame-ancestors':           [
                    "'self'",
                    "https://www.googletagmanager.com",
                    "https://www.google-analytics.com",
                ],
                'connect-src':               [
                    "'self'",
                    "https://www.googletagmanager.com",
                    "https://www.google-analytics.com",
                ],
                'upgrade-insecure-requests': true,
            }
        },
        nonce:         true,
        ssg:           {
            meta:        true,
            hashScripts: true,
            hashStyles:  true,
        },
        sri:           true,
        hidePoweredBy: true,
        removeLoggers: {
            external:    [],
            consoleType: ['log', 'debug'],
            include:     [/\.[jt]sx?$/, /\.vue\??/],
            exclude:     [/node_modules/, /\.git/],
        }
    },
    lodash:        {
        prefix:           "_",
        prefixSkip:       ["string"],
        upperAfterPrefix: false,
        exclude:          ["map"],
        alias:            [
            ["camelCase", "stringToCamelCase"], // => stringToCamelCase
            ["kebabCase", "stringToKebab"], // => stringToKebab
            ["isDate", "isLodashDate"], // => _isLodashDate
        ],
    },
    devServer:     {
        https: {
            key:  './.ssl/_wildcard.local.devnet.kz+4-key.pem',
            cert: './.ssl/_wildcard.local.devnet.kz+4.pem',
        },
        port:  3202,
    },
    sourcemap:     true,
    robots:        {
        rules: {
            UserAgent: '*',
            Disallow:  '',
        },
    },
    gtag:          {
        enabled: false,
    }
})
