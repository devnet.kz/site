import type {Config} from 'tailwindcss'

export default <Partial<Config>>{
  content: [
    "./src/components/**/*.{vue,ts}",
    "./src/layouts/**/*.vue",
    "./src/pages/**/*.vue",
    "./src/plugins/**/*.ts",
    "./src/error.vue",
    "./content/**/*.md",
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}
