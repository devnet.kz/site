---
title: Golang code live reload (Горячая перегрузка кода)
short: Golang code live reload (Горячая перегрузка кода)
description: Как можно сделать "Горячую перегрузку кода".
createdAt: 2022-06-04
updatedAt: 2022-06-17
category: golang
isDraft: false
slug: golang-code-live-reload
author: Yerlen Zhubangaliyev
email: yerlen@yerlen.com
pin: false
buymeacoffee: https://www.buymeacoffee.com/teamxg
linkedin: https://www.linkedin.com/in/yerlen-zhubangaliyev/
gitlab: 
tags:
- golang
- air
- live-reload
- livereload
- hot-reload
- hotreload
- docker
- docker-compose
- Dockerfile
image: /images/covers/golang-logo.svg
license:
- by
- nc
language:
- ru
---

# Golang - Горячая перегрузка кода

При разработке я часто использую замечательный инструмент ``air`` - [github.com/cosmtrek/air](https://github.com/cosmtrek/air).

Также подразумевается что вы используете Docker в разработке.

## В существующий или новый проект добавьте/модифицируйте ``Dockerfile``

```dockerfile[Dockerfile]
FROM golang:1.18-bullseye

WORKDIR /app

RUN go install github.com/cosmtrek/air@latest

CMD ["air", "-d"]
```

## Добавьте/модифицируйте ``docker-compose.yml``

Приложение будет запускаться на порту 8800

```yaml[docker-compose.yml]
version: "3.9"

services:
  app:
    build:
      context:    .
      dockerfile: Dockerfile
    working_dir: /app
    ports:
      - "8800:8800"
    volumes:
      - ./:/app
```

## Создайте конфигурационный файл ``.air.toml``

В моих приложениях файлы инициализации приложений находятся в папке ``/cmd``.
В нашем случае путь к файлу будет ``/cmd/app/main.go``.

С содержанием:
```go[cmd/app/main.go]
package main

import "fmt"

func main() {
    fmt.Println("Hello")
}
```

В данном конфигурационном файле мы укажем:

* root_dir = "."
* tmp_dir = ".tmp"
* cmd = "go build -o .tmp ./cmd/app"

При запуске ``docker compose up`` - будет создана папка ``.tmp`` и скомпилированное приложение ``.tmp/app``.

```toml[.air.toml]
# Config file for [Air](https://github.com/cosmtrek/air) in TOML format

# Working directory
# . or absolute path, please note that the directories following must be under root.
root = "."
tmp_dir = ".tmp"

[build]
# Just plain old shell command. You could use `make` as well.
cmd = "go build -o .tmp ./cmd/app"
# Binary file yields from `cmd`.
bin = ".tmp/app"
# Customize binary, can setup environment variables when run your app.
full_bin = "./.tmp/app"
# Watch these filename extensions.
include_ext = ["go", "tpl", "tmpl", "html", "local"]
# Ignore these filename extensions or directories.
exclude_dir = [".tmp"]
# Watch these directories if you specified.
include_dir = []
# Exclude files.
exclude_file = []
# Exclude specific regular expressions.
exclude_regex =  ["_test.go"]
# Exclude unchanged files.
exclude_unchanged = true
# Follow symlink for directories
follow_symlink = true
# This log file places in your tmp_dir.
log = ".tmp/air.log"
# It's not necessary to trigger build each time file changes if it's too frequent.
delay = 500 # ms
# Stop running old binary when build errors occur.
stop_on_error = true
# Send Interrupt signal before killing process (windows does not support this feature)
send_interrupt = false
# Delay after sending Interrupt signal
kill_delay = 500 # ms

[log]
# Show log time
time = true

[color]
# Customize each part's color. If no color found, use the raw app log.
main = "magenta"
watcher = "cyan"
build = "yellow"
runner = "green"

[misc]
# Delete tmp directory on exit
clean_on_exit = true
```

## Запускаем приложение с ``Air``

```bash
docker compose up
```

Выйдет примерно такое:

```bash
Attaching to app-1
app-1         | 
app-1         |   __    _   ___  
app-1         |  / /\  | | | |_) 
app-1         | /_/--\ |_| |_| \_ , built with Go 
app-1         | 
app-1         | [debug] mode
app-1         | [10:00:19] CWD: /app
app-1         | [10:00:19] mkdir /app/.tmp
app-1         | [10:00:19] watching .
......................
app-1         | [10:00:19] running...
app-1         | [10:00:19] running process pid 88
app-1         | Hello
```

## Выводы

В данной мини-статье мы показали как удобнее разрабатывать приложение на Golang с горячей перегрузкой.
