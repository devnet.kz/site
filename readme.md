# Devnet.kz/Foobar.kz main site

based on nuxt.js 2.x/3.x, nuxt/content and tailwindcss

## Development

### V2 (PORT: 3200) `(NUXT 2)`

```bash
make init-v2
```

based on nuxt.js 3.x, @nuxt/content and tailwindcss/daisyui

### V3 (PORT: 3201) `(NUXT 3)`

```bash
make init-v3
```

### V4 (PORT: 3202) `(NUXT 3)`

1. Install `mkcert` -> https://github.com/FiloSottile/mkcert
and then

```bash
mkdir -p devnet.kz/v4/.ssl
cd devnet.kz/v4/.ssl
mkcert -install
mkcert "*.local.devnet.kz" "*.local.foobar.kz" localhost 127.0.0.1 ::1
```

2. Init

```bash
make init-v4
```

3. Open https://site.local.foobar.kz:3202 | https://site.local.devnet.kz:3202
