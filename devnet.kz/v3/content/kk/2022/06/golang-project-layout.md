---
title: Golang қолданбасының құрылымы (Commandline + RESTFul API)
short: Golang қолданбасының құрылымы (Commandline + RESTFul API)
description: Басқа әзірлеушілер жобамен жұмыс істеп, оны контейнерлерге жинап, өндіріске жеткізетін шартпен қолданба құрылымын қалай дұрыс ұйымдастыруға болады.
createdAt: 2022-06-02
category: golang
isDraft: false
slug: golang-project-layout
author: Yerlen Zhubangaliyev
email: yerlen@yerlen.com
pin: false
buymeacoffee: https://www.buymeacoffee.com/teamxg
linkedin: https://www.linkedin.com/in/yerlen-zhubangaliyev/
gitlab:
tags:
  - golang
image: /images/covers/golang-logo.svg
license:
  - by
  - nc
language:
  - ru
  - kk
---

# Golang структура приложения (Commandline + RESTFul API)

Чаще всего, прежде чем начать новый проект - мы начинаем задумываться - как организовать правильно структуру приложения,
с условием того, что с проектом будут работать, другие разработчики, собираться в контейнеры и доставляться в production.

Наше приложение будет реализовывать RESTFul API + Command line (для запуска различных обработчиков работающих в фоне).

В среде разработчиков на Golang - довольно популярен проект [github.com/golang-standards/project-layout](https://github.com/golang-standards/project-layout).
Конечно это не стандарт от разработчиков самого языка программирования - это сообщество, поэтому
некоторым рекомендациям можно не следовать для своего удобства.

## Создадим пустой проект `golang-project-layout-api-cli`

### Базовые широко используемые директории

#### /cmd

Базовая директория для ваших `main.go` файлов, в которых вы инициализируете ваши приложения.

К примеру, у нас 2 типа приложения `API` и `CLI`:

```code,bash
cmd/
├─ api/
│  ├─ main.go
├─ cli/
│  ├─ main.go
```

#### /internal

Базовая директория для приватного кода и библиотек.

К примеру:

```code,bash
internal/
├─ app/
│  ├─ api/
│  │  ├─ controllers/
│  │  │  ├─ ...
│  │  ├─ handlers/
│  │  │  ├─ ...
│  │  ├─ validations/
│  │  ├─ ...
│  ├─ cli/
│  │  ├─ database/
│  │  │  ├─ migrations/
│  │  │  │  ├─ ...
│  │  ├─ commands/
│  │  │  ├─ ...
├─ pkg/
│  ├─ libuuid/
│  │  ├─ ...
│  ├─ libpki/
│  │  ├─ ...
│  ├─ libjson/
│  │  ├─ ...
```

#### /pkg

Данную директорию в основном используют для кода который может импортирован другими проектами.

К примеру:

```code,bash
pkg/
├─ libuuid/
│  ├─ ...
├─ libpki/
│  ├─ ...
├─ libjson/
│  ├─ ...
```

#### /vendor

Эта директория для внешних зависимостей, которые могут использоваться в вашем приложении.
Чаще всего управляется вручную.
Важно заметить когда вы будете собирать приложение с такими зависимостями добавляйте опцию `-mod=vendor`.

К примеру:

```code,bash
vendor/
├─ mycompanylib/
│  ├─ main.go
├─ myothercompanylib/
│  ├─ main.go
```

Также мы возьмем некоторые директории, которые рекомендует сообщество:

`/init` - для размещения конфигураций инициализации CLI (supervisor.d, system.d)

`/configs` - для размещения конфигурационных файлов

`/deployments` - для размещения конфигурационных файлов систем контейнерной оркестрации (Docker swarm, K8s)

`/build` - для размещения конфигурационных файлов CI/CD

Про другие рекомендуемые директории читайте [RU - github.com/golang-standards/project-layout](https://github.com/golang-standards/project-layout/blob/master/README_ru.md)

## Выводы

В данной статье мы раскрыли наиболее лучшую практику структуры приложения RESTFul API + Command line.

В следующей статье "обогатим" кодом и реальным использованием.
