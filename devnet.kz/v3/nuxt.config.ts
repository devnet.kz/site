import svgLoader from 'vite-svg-loader'
import pugPlugin from 'vite-plugin-pug'
// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
    typescript: {
        strict:    true,
        typeCheck: false,
    },
    modules:    [
        '@nuxt/content',
        '@nuxtjs/tailwindcss',
        '@nuxtjs/color-mode',
        '@nuxt/devtools',
        'nuxt-icon',
        '@nuxtjs/google-fonts',
        '@nuxt/image-edge',
        '@nuxtjs/i18n',
    ],
    i18n:       {
        langDir:               'locales',
        locales:               [
            {
                code: 'en',
                iso:  'en-US',
                file: 'en.json',
                name: 'English',
            },
            {
                code: 'ru',
                iso:  'ru-RU',
                file: 'ru.json',
                name: 'Русский',
            },
            {
                code: 'kk',
                iso:  'kk-KZ',
                file: 'kk.json',
                name: 'Қазақ тілі',
            },
        ],
        lazy:                  true,
        detectBrowserLanguage: {
            useCookie:  true,
            redirectOn: 'root',
        },
        strategy:              'no_prefix',
        defaultLocale:         'en',
        baseUrl:               process.env.BASE_URL || 'https://localhost:3000',
    },

    colorMode: {classSuffix: '', dataValue: 'theme'},
    extends:   ['@nuxt-themes/elements'],

    content: {
        locales:   ['en', 'ru', 'kk'],
        markdown:  {
            remarkPlugins: ['remark-reading-time'],
        },
        highlight: {
            theme:   {
                default: 'github-dark',
                dark:    'github-dark',
            },
            preload: [
                'json',
                'js',
                'ts',
                'html',
                'css',
                'vue',
                'diff',
                'shell',
                'markdown',
                'yaml',
                'bash',
                'ini',
                'docker',
                'c',
                'cpp',
                'cmake',
                'dart',
                'elixir',
                'go',
                'graphql',
                'haskell',
                'java',
                'nginx',
                'php',
                'pug',
                'python',
                'ruby',
                'rust',
                'vim',
            ],
        },
    },

    googleFonts: {
        families:      {
            Montserrat:  true,
            'Fira+Code': true,
        },
        prefetch:      true,
        preconnect:    true,
        preload:       true,
        useStylesheet: true,
    },

    vite: {
        plugins: [pugPlugin(), svgLoader()],
        server:  {
            strictPort: true,
            hmr:        {
                port: 9005
            },
        },
    },
})
